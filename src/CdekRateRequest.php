<?php


namespace Drupal\commerce_cdek;


use Drupal\commerce_shipping\Entity\ShipmentInterface;

class CdekRateRequest implements CdekRateRequestInterface {

  /**
   * @inheritDoc
   */
  public function getRates(ShipmentInterface $shipment, array $config) {

    $rateService = new CdekRateService($config);
    $rateService->setAuth($config['api_information']['api_key'], $config['api_information']['api_password']);

    $shipment_items = $shipment->getItems();
    $storage = \Drupal::entityTypeManager()->getStorage('commerce_order_item');

    foreach ($shipment_items as $shipment_item) {
      $order_item_ids[] = $shipment_item->getOrderItemId();
      $weight = $shipment_item->getWeight()->convert('kg')
        ->getNumber();

      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $order_item = $storage->load($shipment_item->getOrderItemId());

      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $purchased_entity */
      $purchased_entity = $order_item->getPurchasedEntity();
      if ($purchased_entity->hasField('dimensions') && !$purchased_entity->get('dimensions')
          ->isEmpty()) {
        /** @var \Drupal\physical\Plugin\Field\FieldType\DimensionsItem $dimensions */
        $dimensions = $purchased_entity->get('dimensions')->first();
        $height = $dimensions->getHeight()->convert($dimensions->getHeight()
          ->getUnit())
          ->getNumber();
        $length = $dimensions->getLength()->convert($dimensions->getLength()
          ->getUnit())->getNumber();
        $width = $dimensions->getWidth()->convert($dimensions->getWidth()
          ->getUnit())
          ->getNumber();
      }
      $rateService->addGoodsItemBySize($weight, $length, $width, $height);
    }

    foreach ($config['current_services'] as $service) {
      $rateService->addTariffPriority($service->getId());
    }

    $sender_address = $shipment->getOrder()->getStore()->getAddress();
    $rateService->setSenderCityPostCode($sender_address->getPostalCode());

    $recipient_address = $shipment->getShippingProfile()
      ->get('address')
      ->first();
    $rateService->setReceiverCityPostCode($recipient_address->getPostalCode());

    return $rateService->calculate();

  }

}