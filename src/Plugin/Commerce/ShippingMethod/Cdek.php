<?php

namespace Drupal\commerce_cdek\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\Core\Form\FormStateInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the CDEK shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "cdek",
 *   label = @Translation("Cdek"),
 *   services = {
 *     "7" = "Международный экспресс документы дверь-дверь",
 *     "8" = "Международный экспресс грузы дверь-дверь",
 *     "136" = "Посылка склад-склад",
 *     "137" = "Посылка склад-дверь",
 *     "138" = "Посылка дверь-склад",
 *     "139" = "Посылка дверь-дверь",
 *     "233" = "Экономичная посылка склад-дверь",
 *     "234" = "Экономичная посылка склад-склад",
 *     "291" = "CDEK Express склад-склад",
 *     "293" = "CDEK Express дверь-дверь",
 *     "294" = "CDEK Express склад-дверь",
 *     "295" = "CDEK Express дверь-склад",
 *   }
 * )
 */
class Cdek extends ShippingMethodBase {

  /**
   * The Service Plugins.
   *
   * @var \Drupal\Core\Plugin\DefaultLazyPluginCollection
   */
  protected $plugins;

  /**
   * @var \Drupal\commerce_nzpost\RateLookupService
   */
  protected $rateRequestService;

  /**
   * Constructs a new ShippingMethodBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   *   The CDEK Request Service.
   * @param \Drupal\commerce_cdek\CdekRateRequest $rate_request
   *   RateLookupService.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager, $rate_request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);
    $this->rateRequestService = $rate_request;
    if (empty($this->configuration['plugins'])) {
      $this->configuration['plugins'] = [];
    }
    $this->configuration['current_services'] = $this->getServices();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('commerce_cdek.rate_request')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {

    return [
        'api_information' => [
          'api_key' => 'EMscd6r9JnFiQ3bLoyjJY6eM78JrJceI',
          'api_password' => 'PjLZkKBHEiLK3YsjtNrt3TGNG0ahs3kG',
          'mode' => 'test',
        ],
        'cdek_auth' => [],
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   *
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildConfigurationForm($form, $form_state);

    if (empty($this->configuration['services'])) {
      $service_ids = array_keys($this->services);
      $this->configuration['services'] = array_combine($service_ids, $service_ids);
    }

    $form['api_information'] = [
      '#type' => 'details',
      '#title' => $this->t('API information'),
      '#description' => $this->isConfigured() ? $this->t('Update your CDEK API information.') : $this->t('Fill in your CDEK API information.'),
      '#weight' => $this->isConfigured() ? 10 : -10,
      '#open' => !$this->isConfigured(),
    ];
    $form['api_information']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => $this->t('Enter your CDEK API key.'),
      '#default_value' => $this->configuration['api_information']['api_key'],
    ];
    $form['api_information']['api_password'] = [
      '#type' => 'password',
      '#title' => $this->t('API password'),
      '#description' => $this->t('Enter your CDEK API password only if you wish to change its value.'),
      '#default_value' => '',
    ];
    $form['api_information']['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Mode'),
      '#description' => $this->t('Choose whether to use the test or live mode.'),
      '#options' => [
        'test' => $this->t('Test'),
        'live' => $this->t('Live'),
      ],
      '#default_value' => $this->configuration['api_information']['mode'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['api_information']['api_key'] = $values['api_information']['api_key'];
      if (!empty($values['api_information']['api_password'])) {
        $this->configuration['api_information']['api_password'] = $values['api_information']['api_password'];
      }
      $this->configuration['api_information']['mode'] = $values['api_information']['mode'];
      unset($this->configuration['plugins']);
    }
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Determine if we have the minimum information to connect to FedEx.
   *
   * @return bool
   *   TRUE if there is enough information to connect, FALSE otherwise.
   */
  protected function isConfigured() {
    $api_information = $this->configuration['api_information'];

    return (
      !empty($api_information['api_key'])
      && !empty($api_information['api_password'])
    );
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment) {

    if ($shipment->getShippingProfile()->get('address')->isEmpty()) {
      return [];
    }

    if (empty($shipment->getPackageType())) {
      $shipment->setPackageType($this->getDefaultPackageType());
    }

    $result = $this->rateRequestService->getRates($shipment, $this->configuration);

    $rates = [];
    foreach ($result as $tariff) {
      if (!empty($tariff['tariffId']) && !empty($tariff['status'])) {
        if (array_key_exists($tariff['tariffId'], $this->getServices())) {
          $amount = Price::fromArray([
            'number' => $tariff['result']['price'],
            'currency_code' => $tariff['result']['currency'],
          ]);
          $rates[] = new ShippingRate([
            'shipping_method_id' => $this->parentEntity->id(),
            'service' => $this->services[$tariff['tariffId']],
            'amount' => $amount,
          ]);
        }
      }
    }
    return $rates;
  }

}
