<?php


namespace Drupal\commerce_cdek;


use Drupal\commerce_shipping\Entity\ShipmentInterface;

interface CdekRateRequestInterface {

  /**
   * Fetch rates for the shipping method.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The commerce shipment.
   *
   * @param array $config
   *
   * @return array
   *   An array of ShippingRate objects.
   */
  public function getRates(ShipmentInterface $shipment, array $config);
}