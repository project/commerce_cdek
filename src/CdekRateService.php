<?php


namespace Drupal\commerce_cdek;


use GuzzleHttp\Client;

class CdekRateService {

  /**
   * Array of departure locations
   *
   * @var array
   */
  public $goodsList = [];

  /**
   * List of tariffs with priorities
   *
   * @var array
   */
  public $tariffList = [];

  /**
   * Planned order date
   *
   * @var false|string
   */
  public $dateExecute;

  /**
   * Module version
   *
   * @var string
   */
  private $version = "1.0";

  /**
   * Auth login
   *
   * @var
   */
  private $authLogin;

  /**
   * Auth password
   *
   * @var
   */
  private $authPassword;

  /**
   * Sender city id
   *
   * @var
   */
  private $senderCityId;

  /**
   * Recipient city id
   *
   * @var
   */
  private $receiverCityId;

  /**
   * Tariff
   *
   * @var
   */
  private $tariffId;

  /**
   * Delivery mode
   *
   * @var
   */
  private $modeId;

  /**
   * Configuration
   *
   * @var array
   */
  private $config;

  public function __construct(array $config) {
    $this->dateExecute = date('Y-m-d');
    $this->config = $config;
  }

  /**
   * Setting the planned shipping date
   * Expample: '2012-06-25'
   *
   * @param string $date
   */
  public function setDateExecute($date) {
    $this->dateExecute = date($date);
  }

  /**
   * Set auth
   *
   * @param string $authLogin
   * @param string $authPassword
   */
  public function setAuth($authLogin, $authPassword) {
    $this->authLogin = $authLogin;
    $this->authPassword = $authPassword;
  }

  /**
   * Set sender city by id
   *
   * @param int $id города
   */
  public function setSenderCityId($id) {
    $id = (int) $id;
    if ($id == 0) {
      throw new \Exception("Invalid sending city.");
    }
    $this->senderCityId = $id;
  }

  /**
   * Set recipient city id
   *
   * @param int $id города
   */
  public function setReceiverCityId($id) {
    $id = (int) $id;
    if ($id == 0) {
      throw new \Exception("Wrong destination city.");
    }
    $this->receiverCityId = $id;
  }

  /**
   * Set sender city by post code
   *
   * @param int $code
   */
  public function setSenderCityPostCode($code) {
    $id = (int) $code;
    if ($id == 0) {
      throw new \Exception("The index of the sender city is set incorrectly");
    }
    $this->senderCityPostCode = $id;
  }

  /**
   * Set recipient city by post code
   *
   * @param int $code
   */
  public function setReceiverCityPostCode($code) {
    $id = (int) $code;
    if ($id == 0) {
      throw new \Exception("The index of the destination city is set incorrectly.");
    }
    $this->receiverCityPostCode = $id;
  }

  /**
   * Set tariff
   *
   * @param int $id тарифа
   */
  public function setTariffId($id) {
    $id = (int) $id;
    if ($id == 0) {
      throw new \Exception("Tariff is set incorrectly.");
    }
    $this->tariffId = $id;
  }

  /**
   * Set delivery mode
   *
   * @param int $id
   */
  public function setModeDeliveryId($id) {
    $id = (int) $id;
    if (!in_array($id, [1, 2, 3, 4])) {
      throw new \Exception("Delivery mode is set incorrectly.");
    }
    $this->modeId = $id;
  }

  /**
   * Adding a place in a shipment
   *
   * @param int $weight вес, килограммы
   * @param int $length длина, сантиметры
   * @param int $width ширина, сантиметры
   * @param int $height высота, сантиметры
   */
  public function addGoodsItemBySize($weight, $length, $width, $height) {
    $weight = (float) $weight;
    if ($weight == 0.00) {
      throw new \Exception("Place weight is set incorrectly № " . (count($this->getGoodslist()) + 1) . ".");
    }
    $paramsItem = [
      'length' => $length,
      'width' => $width,
      'height' => $height,
    ];
    foreach ($paramsItem as $k => $param) {
      $param = (int) $param;
      if ($param == 0) {
        throw new \Exception("The parameter is set incorrectly '" . $k . "' № " . (count($this->getGoodslist()) + 1) . ".");
      }
    }
    $this->goodsList[] = [
      'weight' => $weight,
      'length' => $length,
      'width' => $width,
      'height' => $height,
    ];
  }

  /**
   * Adding space in a shipment by volume
   *
   * @param int $weight weight, kilograms
   * @param int $volume volume weight, cubic meters (А * В * С)
   */
  public function addGoodsItemByVolume($weight, $volume) {
    $paramsItem = [
      'weight' => $weight,
      'volume_weight' => $volume,
    ];
    foreach ($paramsItem as $k => $param) {
      $param = (float) $param;
      if ($param == 0.00) {
        throw new \Exception("The parameter is set incorrectly '" . $k . "' № " . (count($this->getGoodslist()) + 1) . ".");
      }
    }
    $this->goodsList[] = [
      'weight' => $weight,
      'volume' => $volume,
    ];
  }

  /**
   * Receiving an array of departure locations
   *
   * @return array
   */
  public function getGoodslist() {
    if (!isset($this->goodsList)) {
      return NULL;
    }
    return $this->goodsList;
  }

  /**
   * Adding a tariff to the list of tariffs with priorities
   *
   * @param int $id tariff
   * @param int $priority default false priority
   */
  public function addTariffPriority($id, $priority = 0) {
    $id = (int) $id;
    if ($id == 0) {
      throw new \Exception("Tariff id is set incorrectly.");
    }
    $priority = ($priority > 0) ? $priority : count($this->tariffList) + 1;
    $this->tariffList[] = [
      'priority' => $priority,
      'id' => $id,
    ];
  }

  /**
   * Delivery cost calculation
   *
   * @return array|bool
   */
  public function calculate() {
    $data = [];
    $data['version'] = !empty($this->version) ? $this->version : '';
    $data['dateExecute'] = $this->dateExecute;
    $data['authLogin'] = !empty($this->authLogin) ? $this->authLogin : '';
    $data['secure'] = !empty($this->authPassword) ? $this->getSecureAuthPassword() : '';
    $data['senderCityId'] = !empty($this->senderCityId) ? $this->senderCityId : '';
    $data['senderCityPostCode'] = !empty($this->senderCityPostCode) ? $this->senderCityPostCode : '';
    $data['receiverCityId'] = !empty($this->receiverCityId) ? $this->receiverCityId : '';
    $data['receiverCityPostCode'] = !empty($this->receiverCityPostCode) ? $this->receiverCityPostCode : '';
    $data['tariffId'] = !empty($this->tariffId) ? $this->tariffId : '';
    $data['tariffList'] = !empty($this->tariffList) ? $this->tariffList : '';
    $data['modeId'] = !empty($this->modeId) ? $this->modeId : '';

    if (!empty($this->goodsList)) {
      foreach ($this->goodsList as $idGoods => $goods) {
        $data['goods'][$idGoods] = [];
        (!empty($goods['weight']) && $goods['weight'] <> '' && $goods['weight'] > 0.00) ? $data['goods'][$idGoods]['weight'] = $goods['weight'] : '';
        (!empty($goods['length']) && $goods['length'] <> '' && $goods['length'] > 0) ? $data['goods'][$idGoods]['length'] = $goods['length'] : '';
        (!empty($goods['width']) && $goods['width'] <> '' && $goods['width'] > 0) ? $data['goods'][$idGoods]['width'] = $goods['width'] : '';
        (!empty($goods['height']) && $goods['height'] <> '' && $goods['height'] > 0) ? $data['goods'][$idGoods]['height'] = $goods['height'] : '';
        (!empty($goods['volume']) && $goods['volume'] <> '' && $goods['volume'] > 0.00) ? $data['goods'][$idGoods]['volume'] = $goods['volume'] : '';
      }
    }

    $data = array_filter($data);
    $response = $this->getRemoteData($data);

    if (!empty($response['result'])) {
      return $response['result'];
    }

    return FALSE;
  }

  /**
   * Encrypted password
   *
   * @return string
   */
  private function getSecureAuthPassword() {
    return md5($this->dateExecute . '&' . $this->authPassword);
  }

  /**
   * Obtaining an array of specified tariffs
   *
   * @return array
   */
  private function getTariffList() {
    return $this->tariffList;
  }

  private function getRemoteData($data) {

    $options = [
      'headers' => [
        'Content-Type' => 'application/json',
      ],
      'base_uri' => ($this->config['api_information']['mode'] == 'test') ? 'http://api.edu.cdek.ru' : 'http://api.cdek.ru',
    ];

    $client = new Client($options);

    $response = $client->post('/calculator/calculate_tarifflist.php', [
      'json' => $data,
    ]);
    $result = $response->getBody()->__toString();
    return json_decode($result, TRUE);
  }

}
